/**
 * @fileoverview Gruntfile.js
 * @author ryosuke.tsuji
 * @version 1.0.0
 */

'use strict';

module.exports = function(grunt) {
    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    grunt.initConfig({
        path: {
            'src':   'src',
            'js':    'src/js',
            'views': 'src/_views',
            'css':   'src/_styl',
            'data':  'src/_data',
            'bower': 'bower_components',
            'dist':  'dist',
            'tmp': '.tmp',
            'demo':  'demo',
            'doc':   'docs'
        },
        esteWatch: {
            options: {
                dirs: ['<%= path.src %>/**'],
                livereload: {
                    enabled: true,
                    extensions: [
                        'html',
                        'jade',
                        'json',
                        'styl',
                        'js'
                    ],
                    port: 35742
                }
            },
            html: function() {
                return ['copy:doc'];
            },
            jade: function() {
                return ['jade'];
            },
            json: function() {
                return ['jade'];
            },
            styl: function() {
                return ['stylus', 'autoprefixer'];
            },
            js: function() {
                return ['uglify', 'copy:doc', 'copy:demo'];
            }
        },
        uglify: {
            options: {
                ascii_only: true,
                preserveComments: 'some',
                banner: '/**' + '\n' +
                     '* @fileOverview optimize entry form' + '\n' +
                     '* @description depend on jQuery' + '\n' +
                     '* @name efo.js' + '\n' +
                     '* @author ryosuke.tsuji ryosuke.tsuji@mail.rakuten.com' + '\n' +
                     '* @version 1.0.0' + '\n' +
                     '* Copyright (c) 2014 "thujikun" Ryosuke Tsuji' + '\n' +
                     '* Licensed under the MIT license.' + '\n' +
                    '*/'
            },
            core: {
                files: {
                    '<%= path.dist %>/efo.core.min.js': [
                        '<%= path.bower %>/input-supporter/dist/input-supporter.core.min.js',
                        '<%= path.bower %>/validator/dist/validator.js'
                    ]
                }
            },
            all: {
                files: {
                    '<%= path.dist %>/efo.all.min.js': [
                        '<%= path.js %>/polyfill.js',
                        '<%= path.bower %>/input-supporter/dist/input-supporter.core.min.js',
                        '<%= path.bower %>/validator/dist/validatorjs'
                    ]
                }
            },
            demo: {
                files: {
                    '<%= path.demo %>/efo.all.min.js': [
                        '<%= path.js %>/polyfill.js',
                        '<%= path.bower %>/input-supporter/dist/input-supporter.core.min.js',
                        '<%= path.bower %>/validator/dist/validator.js'
                    ]
                }
            },
            doc: {
                files: {
                    '<%= path.doc %>/js/efo.all.min.js': [
                        '<%= path.js %>/polyfill.js',
                        '<%= path.bower %>/input-supporter/dist/input-supporter.core.min.js',
                        '<%= path.bower %>/validator/dist/validator.js'
                    ]
                }
            },
        },
        jade: {
            src: {
                files: [{
                    expand: true,
                    cwd: '<%= path.views %>',
                    src: ['{,**/}*.jade', '!**/_*'],
                    dest: '<%= path.demo %>',
                    ext: '.html'
                }],
                options: {
                    client: false,
                    pretty: true,
                    basedir: '<%= path.views %>',
                    data: function(dest, src) {
                        var page = src[0].replace(/drc\/_views\/(.*)\/index.jade/, '$1');
                        var validationConfig = require('./' + grunt.config.data.path.data + '/validator.json');
                        var inputSupportConfig = require('./' + grunt.config.data.path.data + '/supporter.json');

                        if (page == src[0]) {
                            page = 'index';
                        }

                        return {
                            page: page,
                            validationConfig: validationConfig,
                            inputSupportConfig: inputSupportConfig
                        };
                    }
                }
            }
        },
        stylus: {
            demo: {
                files: [{
                    expand: true,
                    cwd: '<%= path.css %>',
                    src: ['{,**/}*.styl', '!**/_*'],
                    dest: '<%= path.tmp %>/css',
                    ext: '.css'
                }],
                options: {
                    compress: false,
                    urlfunc: 'url'
                }
            },
            doc: {
                files: [{
                    expand: true,
                    cwd: '<%= path.src %>/docs/_styl',
                    src: ['{,**/}*.styl', '!**/_*'],
                    dest: '<%= path.tmp %>/css',
                    ext: '.css'
                }],
                options: {
                    compress: false,
                    urlfunc: 'url'
                }
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= path.css %>',
                    src: ['{,**/}efo.styl'],
                    dest: '<%= path.dist %>/css',
                    ext: '.css'
                }],
                options: {
                    compress: false,
                    urlfunc: 'url'
                }
            },
        },
        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 7', 'ie 8', 'ie 9', 'Android 2']
            },
            demo: {
                files: [{
                    expand: true,
                    cwd: '<%= path.tmp %>',
                    src: ['css/{,**/}*.css'],
                    dest: '<%= path.demo %>'
                }]
            },
            doc: {
                files: [{
                    expand: true,
                    cwd: '<%= path.tmp %>',
                    src: ['css/{,**/}*.css'],
                    dest: '<%= path.doc %>'
                }],
            },
        },
        copy: {
            demo: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= path.src %>',
                    src: ['{,**/}*.*', '!**/_*/{,**/}*.*'],
                    dest: '<%= path.demo %>'
                }]
            },
            doc: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= path.src %>/docs',
                    src: ['{,**/}*.*', '!**/_*/{,**/}*.*'],
                    dest: '<%= path.doc %>'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= path.js %>',
                    src: ['{,**/}execute.js'],
                    dest: '<%= path.dist %>'
                }]
            },
            jquery: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= path.bower %>/jquery/dist/',
                    src: ['jquery.min.js'],
                    dest: '<%= path.demo %>/js/vendor/jquery'
                }]
            },
            jquerydoc: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= path.bower %>/jquery/dist/',
                    src: ['jquery.min.js'],
                    dest: '<%= path.doc %>/js/vendor/jquery'
                }]
            }
        },
        clean: {
            tmp: '<%= path.tmp %>',
            dist: '<%= path.dist %>',
            demo: '<%= path.demo %>',
            doc: '<%= path.doc %>'
        },
        connect: {
            options: {
                port: 9292,
                hostname: '0.0.0.0',
                livereload: 35742,
            },
            demo: {
                options: {
                    open: 'http://localhost:9292/',
                    base: [
                        '<%= path.demo %>',
                        '<%= path.bower %>'
                    ]
                }
            },
            doc: {
                options: {
                    open: 'http://localhost:9292/',
                    base: '<%= path.doc %>'
                }
            }
        },
        'ftp-deploy': {
            preview: {
                auth: {
                    host: 'teamsite01.rakuten.co.jp',
                    port: '21',
                    authKey: 'teamsite',
                },
                src: '<%= path.demo %>',
                dest: '/default/main/kakunin.rakuten/all/WORKAREA/00-PUBLIC/htdocs/rakuten/swd/html5/efo',
                exclusions: ['<%= path.demo %>/**/.DS_Store', '<%= path.demo %>/**/Thumbs.db']
            },
            doc: {
                auth: {
                    host: '10.0.79.50',
                    port: '21',
                    authKey: 'cwd-web',
                },
                src: '<%= path.doc %>',
                dest: '/html5/efo/doc',
                exclusions: ['<%= path.doc %>/**/.DS_Store', '<%= path.doc %>/**/Thumbs.db']
            },
            jsdoc: {
                auth: {
                    host: '10.0.79.50',
                    port: '21',
                    authKey: 'cwd-web',
                },
                src: 'jsdoc/efo',
                dest: '/html5/efo/jsdoc',
                exclusions: ['<%= path.doc %>/**/.DS_Store', '<%= path.doc %>/**/Thumbs.db']
            },
        },
        jsdoc: {
            efo: {
                src: [
                    '<%= path.bower %>/validator/src/js/validator.js',
                    '<%= path.bower %>/input-supporter/src/js/input-supporter.js',
                ],
                options: {
                    destination: 'jsdoc/efo/',
                    configure: 'jsdoc/template/jsdoc.conf.json',
                    template: 'jsdoc/template'
                },
            },
        },
    });

    grunt.registerTask('serve', [
        'clean:tmp',
        'clean:demo',
        'stylus:demo',
        'autoprefixer:demo',
        'jade',
        'copy:demo',
        'copy:jquery',
        'uglify:demo',
        'connect:demo',
        'esteWatch'
    ]);

    grunt.registerTask('doc', [
        'clean:tmp',
        'clean:doc',
        'copy:doc',
        'copy:jquerydoc',
        'stylus',
        'autoprefixer:doc',
        'uglify:doc',
        'connect:doc',
        'esteWatch'
    ]);

    grunt.registerTask('preview', [
        'clean:tmp',
        'clean:demo',
        'stylus:demo',
        'autoprefixer:demo',
        'jade',
        'copy:demo',
        'copy:jquery',
        'uglify:demo',
        'ftp-deploy:preview'
    ]);

    grunt.registerTask('document', [
        'clean:tmp',
        'clean:doc',
        'copy:doc',
        'copy:jquerydoc',
        'stylus',
        'autoprefixer:doc',
        'uglify:doc',
        'jsdoc',
        'ftp-deploy:doc',
        'ftp-deploy:jsdoc',
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'uglify',
        'copy:dist',
        'stylus:dist'
    ]);
};
