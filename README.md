EFO
=======================

Introduction
-----------------
This plugin enables to validate input element easily and add input supporter.

Usage
-----------------
1. HTML parameter pattern

````
<form name="input-form" method="post" action="/" formnovalidate="formnovalidate" data-role="validation">
    <input type="text" name="email" title="email" data-validation='{"unit":{"required": true, "format":"email"}, "relation": [{"type": "same", "key": "email", "role": "body"}]}' data-input-support='{"cancelspace":true}'>
    <input type="text" name="email2" title="email2" data-validation='{"unit":{"required": true, "format":"email"}, "relation": [{"type": "same", "key": "email", "role": "confirmation"}]}' data-input-support='{"cancelspace":true}'>
</form>
````

2. javascript option pattern

````
<form name="input-form" method="post" action="/" formnovalidate="formnovalidate" data-role="validation">
    <input type="text" name="email" title="email">
    <input type="text" name="email2" title="email2">
</form>
````

````
// execute form validator
var $form = $('[data-role=validation]');
$form.validator({}, [
    {
        "name": "email",
        "unit": {
            "required": true,
            "format": "email"
        },
        "relation": [
            {
                "type": "same",
                "key": "email",
                "role": "body"
            }
        ]
    },
    {
        "name": "email2",
        "unit": {
            "required": true,
            "format": "email"
        },
        "relation": [
            {
                "type": "same",
                "key": "email",
                "role": "confirmation"
            }
        ]
    }
);

// execute input supporter
$('[data-input-support]').inputSupporter({}, [
    "email": {
        "cancelspace": true
    },
    "email-confirmation": {
        "cancelspace": true
    }
]);
````

Links
-----------------
[DEMO](http://kakunin.rakuten.ne.jp/rakuten/swd/html5/efo/)
[How to customize EFO library](https://confluence.rakuten-it.com/confluence/display/CWDHTML5/How+to+customize+EFO+Library)

Requirements
-----------------
jQuery-.1.7.2+

Compatibility
-----------------
* Internet Explorer 8+
* Firefox
* Chrome
* Safari
* Mobile Safari
* Android browser
* Android Chrome
  
※ We support only latest version of Firefox/Chrome/Safari